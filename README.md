# What is aws-cli-docer

[Docker](https://docker.io) image for running the [Amazon Web Services Command Line Interface](http://aws.amazon.com/cli/) and [s3cmd](https://github.com/s3tools/s3cmd). It creates a [Docker](https://docker.io) image containing all dependencies needed to run `aws` and `s3cmd` with `node.js` (version 8.10). That way, you can run these tools in a [Docker](https://docker.io) container without setting the dependencies on the host system.

## How to use
There are specific CLI tools within the full range of the AWS CLI tool chain. Each of these has been mapped to a branch and tag so if you only need a specific CLI tool and/or have image size concerns there is something here to meet your needs. If you want all the AWS tool goodies, i.e. AWS CLI, RDS CLI, and S3CMD, then select the `master` branch with the `latest` tag in [Docker Hub](https://hub.docker.com/r/siwawong/aws-cli-s3/). Below are the mappings and usages:


| GitHub Branch | Docker Tag | Usage Example                               |
| ------------- | ---------- | ------------------------------------------- |
| master        | latest     | `docker run -it siwawong/aws-cli-s3:latest` |